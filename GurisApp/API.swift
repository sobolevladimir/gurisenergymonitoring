//
//  API.swift
//  GurisApp
//
//  Created by Andrey Plygun on 10/18/20.
//  Copyright © 2020 Andrey Plygun. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct UserDefaultsKeys {
    static let login = "login"
    static let password = "password"
    static let isLoggedIn = "isLoggedIn"
    static let token = "token"
}

class API {
    private static let baseURL = "http://gurismobile.robosoftenerji.com/api/index.php?"
    private static let loginRoute = "operation=login"
    private static let verifyTokenRoute = "operation=verify-token"
    private static let reportTrendRoute = "operation=report-trend&table_and_columns=MOBILE_APP_CEO$PlantDesc,PlantType,GPS,ActivePower,ReactivePower,AVG_TMP,AVG_WINDSPEED,AVG_DIRECTION&top=30"
    
    static func loginWith(userName: String, password: String, callback: @escaping(_ success: Bool?, _ errorMsg: String?) -> ()) {
        let url = URL(string: baseURL + loginRoute + "&username=\(userName)&password=\(password)")
        Alamofire.request(url!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).response { (response) in
            if let error = response.error {
                callback(nil, error.localizedDescription)
                return
            }
            if let data = response.data {
                let responseString = String(data: data, encoding: .utf8)!
                let encodedData = responseString.dropLast(1).data(using: .utf8)!
                let json = JSON(encodedData)
                print(json)
                if let success = json["Success"].bool, success {
                    let token = json["Token"].stringValue
                    verifyToken(token: token) { (success, errorMsg) in
                        callback(success, nil)
                    }
                } else {
                    callback(nil, json["Message"].string)
                }
            }
        }
    }
    
    static private func verifyToken(token: String, callback: @escaping(_ success: Bool?, _ errorMsg: String?) -> ()) {
        let url = URL(string: baseURL + verifyTokenRoute + "&token=\(token)")
        Alamofire.request(url!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).response { (response) in
            if let error = response.error {
                callback(nil, error.localizedDescription)
                return
            }
            if let data = response.data {
                let responseString = String(data: data, encoding: .utf8)!
                let encodedData = responseString.dropLast(1).data(using: .utf8)!
                let json = JSON(encodedData)
                print(json)
                if let tokenIsValid = json["VerifyToken"].bool, tokenIsValid {
                    UserDefaults.standard.setValue(token, forKey: "token")
                    reportTrend(token: token) { (success, errorMsg) in
                        if let errorMsg = errorMsg {
                            callback(nil, errorMsg)
                            return
                        }
                        callback(success, nil)
                    }
                } else {
                    callback(nil, "Wrong token")
                }
            }
        }
    }
    
    static func reportTrend(token: String, callback: @escaping(_ success: Bool?, _ errorMsg: String?) -> ()) {
        let url = URL(string: baseURL + reportTrendRoute + "&token=\(token)")
        Alamofire.request(url!, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: nil).response { (response) in
            if let error = response.error {
                callback(nil, error.localizedDescription)
                return
            }
            if let data = response.data {
                let responseString = String(data: data, encoding: .utf8)!
                let encodedData = responseString.dropLast(1).data(using: .utf8)!
                let json = JSON(encodedData)
                if let success = json["Success"].bool, success {
                    callback(json["Success"].bool, nil)
                } else {
                    callback(nil, json["Message"].string)
                }
            }
        }
    }
}
