//
//  Extensions.swift
//  GurisApp
//
//  Created by Andrey Plygun on 11/2/20.
//  Copyright © 2020 Andrey Plygun. All rights reserved.
//

import UIKit
import Foundation

extension UIViewController {
    
    func showAlertVC(title: String, description: String, animated: Bool) {
        
        guard viewIfLoaded?.window != nil else {
            print("Error: current controller (\(self)) view is invisible")
            return
        }
        
        guard self.presentedViewController == nil else {
            print("Error: controller: \(self) already presenting view controller: \(self.presentedViewController!)")
            return
        }
        
        let alert = UIAlertController(title: title, message: description, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.destructive, handler: nil))
        
        self.present(alert, animated: animated, completion: nil)
    }
    
    func showAlertVC(title: String, description: String, animated: Bool, actions: [UIAlertAction]) {
        
        guard viewIfLoaded?.window != nil else {
            print("Error: current controller (\(self)) view is invisible")
            return
        }
        
        guard self.presentedViewController == nil else {
            print("Error: controller \(self) already presenting view controller: \(self.presentedViewController!)")
            return
        }
        let alert = UIAlertController(title: title, message: description, preferredStyle: UIAlertController.Style.actionSheet)
        for action in actions {
            alert.addAction(action)
        }
        self.present(alert, animated: animated, completion: nil)
    }
}
