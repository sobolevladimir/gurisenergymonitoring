//
//  WebViewVC.swift
//  GurisApp
//
//  Created by Andrey Plygun on 10/18/20.
//  Copyright © 2020 Andrey Plygun. All rights reserved.
//

import UIKit
import WebKit

class WebViewVC: UIViewController {
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var closeMenuButton: UIButton!
    
    var callback: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.load(URLRequest(url: URL(string: "http://guris-test.dstudiosoft.com/mapdata/")!))
        menuView.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        closeMenuButton.imageView?.transform = CGAffineTransform(rotationAngle: .pi)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        guard let login = UserDefaults.standard.value(forKey: UserDefaultsKeys.login) as? String,
            let password = UserDefaults.standard.value(forKey: UserDefaultsKeys.password) as? String else {
                self.logOut()
                self.callback?()
                return
        }
        API.loginWith(userName: login, password: password) { (success, error) in
            if let error = error {
                self.showAlertVC(title: "", description: error, animated: true)
                self.logOut()
                return
            }
            if !success! {
                self.logOut()
                self.callback?()
            }
        }
    }
    
    @IBAction func menuButtonTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.25) {
            self.menuView.frame.origin.x -= 200
        }
    }
    
    @IBAction func hideMenuButtonTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.25) {
            self.menuView.frame.origin.x += 200
        }
    }
    
    @IBAction func logOutButtonTapped(_ sender: Any) {
        let yesAction = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            self.logOut()
        }
        let noAction = UIAlertAction(title: "No", style: .default, handler: nil)
        self.showAlertVC(title: "", description: "Are you sure you want to log out?", animated: true, actions: [yesAction, noAction])
    }
    
    private func logOut() {
        UserDefaults.standard.set(false, forKey: UserDefaultsKeys.isLoggedIn)
        navigationController?.popViewController(animated: true)
    }
}
