//
//  LogInVC.swift
//  GurisApp
//
//  Created by Andrey Plygun on 9/30/20.
//  Copyright © 2020 Andrey Plygun. All rights reserved.
//

//username: demo@guris.com.tr
//password: 123456Aa

import UIKit

class LogInVC: UIViewController {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationStyle = .fullScreen
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewDidTapped)))
        activityIndicator.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        #if DEBUG
            loginTextField.text = "demo@guris.com.tr"
            passwordTextField.text = "123456Aa"
        #endif
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let isLoggedIn = UserDefaults.standard.bool(forKey: UserDefaultsKeys.isLoggedIn)
        if isLoggedIn {
            showWebViewVC()
        }
    }

    @IBAction func loginButtonTapped(_ sender: Any) {
        activityIndicator.isHidden = false
        API.loginWith(userName: loginTextField.text!, password: passwordTextField.text!) { [weak self] (response, errorMsg) in
            self?.activityIndicator.isHidden = true
            if let errorMsg = errorMsg {
                self?.showAlertVC(title: "", description: errorMsg, animated: true)
                return
            }
            if let response = response, response {
                UserDefaults.standard.set(true, forKey: UserDefaultsKeys.isLoggedIn)
                UserDefaults.standard.set(self?.loginTextField.text!, forKey: UserDefaultsKeys.login)
                UserDefaults.standard.set(self?.passwordTextField.text!, forKey: UserDefaultsKeys.password)
                self?.showWebViewVC()
            }
        }
    }
    
    private func showWebViewVC() {
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        vc.callback = {
            self.showAlertVC(title: "", description: "Login or password is incorrect", animated: true)
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if view.frame.origin.y == 0 {
            self.view.frame.origin.y -= 100
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc func viewDidTapped() {
        view.endEditing(true)
    }
}

